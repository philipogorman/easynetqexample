﻿
using System.Threading;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using EasyNetQ;
using Infrastructure;
using Topshelf;
using log4net.Config;
using NLog;

namespace EasyNetQExample
{
    class Program
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private IWindsorContainer _container;
        private IBus _bus;

        /*
         * This project conatins references to CommandHandlers and Remote Service, this is a cheat so that
         * CommandHendelrInstaller can easily resolve these assemblies at runtime. 
         */

        public static void Main(string[] args)
        {
            Thread.CurrentThread.Name = "Domain Service Main Thread";
            HostFactory.Run(x =>
            {
                x.Service<Program>(s =>
                {
                    s.ConstructUsing(name => new Program());
                    s.WhenStarted(p => p.Start());
                    s.WhenStopped(p => p.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("EasyNetQ Service");
                x.SetDisplayName("EasyNetQService");
                x.SetServiceName("EasyNetQService");
            });
        }


        private void Start()
        {
            BasicConfigurator.Configure(); // for TopShelf until it upgrades
            NLog.Config.SimpleConfigurator.ConfigureForConsoleLogging();

            _logger.Info("setting up domain service, installing components");

            _container = new WindsorContainer()
                .Install(
                    new BusInstaller(),
                    new CommandHandlerInstaller()
                );

            _container.Register(Component.For<IWindsorContainer>().Instance(_container));

            _bus = _container.Resolve<IBus>();

            _logger.Info("application configured, started running");

            var message = new EasyNetQTestMessage { Text = "Hello this is a message from within my assembly" };
            try
            {
                using (var publishChannel = _bus.OpenPublishChannel())
                {
                    publishChannel.Publish(message); // Send a message from withing the same assembly
                }
            }
            catch (EasyNetQException e)
            {
                // the server is not connected
                _logger.Error("the server is not connected", e);
            }


        }

        private void Stop()
        {
            _logger.Info("shutting down Domain Service");
            _container.Release(_bus);
            _container.Dispose();
        }
    }
}
