﻿using System;
using EasyNetQ;
using Infrastructure;

namespace EasyNetQExample
{
    public class TestCommandhandler : IConsume<EasyNetQTestMessage>
    {

        public void Consume(EasyNetQTestMessage message)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(message.Text);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

    }
}
