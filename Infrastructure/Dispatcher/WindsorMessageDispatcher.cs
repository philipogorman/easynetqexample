﻿using Castle.Windsor;
using EasyNetQ;

namespace Infrastructure
{
    public class WindsorMessageDispatcher : IMessageDispatcher
    {
        private readonly IWindsorContainer container;

        public WindsorMessageDispatcher(IWindsorContainer container)
        {
            this.container = container;
        }

        public void Dispatch<TMessage, TConsumer>(TMessage message)
            where TMessage : class
            where TConsumer : IConsume<TMessage>
        {
            var consumer = (IConsume<TMessage>) container.Resolve<TConsumer>();
            try
            {
                consumer.Consume(message);
            }
            finally
            {
                container.Release(consumer);
            }
        }
    }

}
