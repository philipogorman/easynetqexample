﻿
namespace Infrastructure
{
    public interface IMessageSender
    {
        void SendMessage(IMessage message);

    }
}
