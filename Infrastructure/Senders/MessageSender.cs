﻿using System;
using EasyNetQ;

namespace Infrastructure
{
    public class MessageSender : IMessageSender
    {
        private IBus _bus;
        public MessageSender(IBus bus)
        {
            _bus = bus;
        }
        public void SendMessage(IMessage message)
        {
            try
            {
                using (var publishChannel = _bus.OpenPublishChannel())
                {
                    Console.WriteLine("Sending Message :" + message);
                    publishChannel.Publish((dynamic)message);
                }
            }
            catch (EasyNetQException)
            {
                // the server is not connected
            }
        }
    }
}
