﻿using EasyNetQ;

namespace Infrastructure
{
    public class BusBuilder
    {
        public static IBus CreateMessageBus()
        {
            return RabbitHutch.CreateBus("host=localhost;virtualHost=/;username=guest;password=guest");
        }
    }
}
