﻿using System.Reflection;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using EasyNetQ;

namespace Infrastructure
{
	public class CommandHandlerInstaller : IWindsorInstaller
	{
		public void Install(IWindsorContainer container, IConfigurationStore store)
		{

            container.Register(Classes.FromAssemblyNamed("CommandHandlers").BasedOn(typeof(IConsume<>)).WithServiceSelf());
            container.Register(Classes.FromAssemblyNamed("RemoteService").BasedOn(typeof(IConsume<>)).WithServiceSelf());


            var bus = container.Resolve<IBus>();

            var autoSubscriber = new AutoSubscriber(bus, "My_subscription_id_prefix")
            {
                MessageDispatcher = new WindsorMessageDispatcher(container)
            };

            autoSubscriber.Subscribe(Assembly.Load("CommandHandlers"));
            autoSubscriber.Subscribe(Assembly.Load("RemoteService"));
            
		}
	}
}
