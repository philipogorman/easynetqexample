﻿
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using EasyNetQ;

namespace Infrastructure
{
    /// <summary>
    /// Installs the service bus into the container.
    /// </summary>
    public class BusInstaller : IWindsorInstaller
    {

        public BusInstaller()
        {
      
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IBus>().UsingFactoryMethod(BusBuilder.CreateMessageBus).LifestyleSingleton()
                );
        }
    }
}
