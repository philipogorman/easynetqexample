﻿using System.Threading;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Infrastructure;
using Topshelf;


namespace RemoteService
{


    class Program
    {
        private static IWindsorContainer _container;

        private static void Main(string[] args)
        {

            Thread.CurrentThread.Name = "EasyNetQ Remote Thread";
            HostFactory.Run(x =>
                {
                    x.Service<Program>(s =>
                        {
                            s.ConstructUsing(name => new Program());
                            s.WhenStarted(p => p.Start());
                            s.WhenStopped(p => p.Stop());
                        });
                    x.RunAsLocalSystem();

                    x.SetDescription("EasyNetQ Remote Service");
                    x.SetDisplayName("EasyNetQRemoteService");
                    x.SetServiceName("EasyNetQRemoteService");
                });
        }


        private void Start()
        {
             _container = new WindsorContainer()
               .Install(
                   new BusInstaller()
               );


            _container.Register(
                Component.For<IMessageSender>().ImplementedBy(typeof(MessageSender)));

            var messageSender = _container.Resolve<IMessageSender>();

            var message = new EasyNetQTestMessage { Text = "Hello this is a message from a remote service" };

            messageSender.SendMessage(message);
        }

        private void Stop()
        {

        }

    }
}
